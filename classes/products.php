<?php
class Products{
  // query
  private $query = "SELECT
  products.id AS id,
  products.name AS name,
  products.description AS description,
  products.price AS price,
  products_categories.category_id AS category_id,
  images.image_file AS image_file
  FROM products
  INNER JOIN products_categories ON 
  products.id = products_categories.product_id
  LEFT JOIN products_images
  ON products.id = products_images.product_id
  LEFT JOIN images 
  ON products_images.image_id = images.image_id";
  // category
  private $category;
  // products array
  private $products = array();
  public function __construct($category=0){
    $this->category = $category;
    $this->getProducts();
  }
  private function getProducts(){
    if($this->category > 0){
      $this->query = $this->query." ".
      "WHERE products_categories.category_id='$this->category'";
      $result = new Database($this->query);
      $this->products = $result->Data();
    }
    else{
      $result = new Database($this->query);
      $this->products = $result->Data();      
    }
  }
  public function renderProducts(){
    foreach($this->products as $product_item){
      $id = $product_item["id"];
      $name = $product_item["name"];
      $description = $product_item["description"];
      $price = $product_item["price"];
      $image = $product_item["image_file"];
      echo "<div class=\"products\">
      <h4>$name</h4>
      <img src=\"images/$image\">
      <h5 class=\"price\">$price</h5>
      <p>$description</p>
      </div>";
    }
  }
  public function __toString(){
    return $this->renderProducts();
  }
}
?>
<?php
class DatabaseConnection{
  public $connection;
  private $user; 
  private $host;
  private $password;
  private $database; 
  public function __construct(){
    $this->user = getenv('user');
    $this->host = getenv('host');
    $this->password = getenv('password');
    $this->database = getenv('database');
    $this->connect();
  }
  private function connect(){
    $this->connection = mysqli_connect(
      $this->host,
      $this->user,
      $this->password,
      $this->database
      );
      if($this->connection){
        return $this->connection;
      }
      else {
        return false;
      }
  }
}
?>
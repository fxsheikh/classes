<?php
$page_title = "Register for an account";
include("includes/database.php");

session_start();

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $useremail = $_POST["email"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    // array of errors
    $errors = array();
    // validate email address
    if(!filter_var($useremail,FILTER_VALIDATE_EMAIL)){
        $errors["email"] = "Email address is invalid";
    }
    // check the username length
    if(strlen($username) < 6 || strlen($username)>16){
        $errors["username"] = "Username needs to be more than 6 characters and less than 16";
    }
    // remove spaces from username
    $nospace = str_replace(" ","",$username);
    if(!ctype_alnum($nospace)){
        $errors["username"] = $errors["username"]." "."Only A-Z,a-z,0-9 allowed";
    }
    // check password length
    if(strlen($password)<8){
        $errors["password"] = "Password should be a minimum of 8 characters";
    }
    //if there are no errors proceed with registration
    if(count($errors)==0){
        // register user, etc.
        // sanitize the values except password
       
        $cleanemail = filter_var($useremail,FILTER_SANITIZE_EMAIL);
        // check if the email already used
        $email_query = "SELECT email FROM accounts WHERE email='$cleanemail'";
        $email_result = $connection->query($email_query);
        if($email_result->num_rows > 0){
            // the email already exists in the database
            $errors["email"] = "email already used";
        }
        
        $cleanuser = filter_var($username,FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
        // check if the username already used
        $username_query = "SELECT username FROM accounts WHERE username='$cleanuser'";
        $username_result = $connection->query($username_query);
        if($username_result->num_rows>0){
            // the username already exists in the database
            $errors["username"] = "username already exists";
        }
        
        if(count($errors)==0){
            // hash the password
            $hash = password_hash($password,PASSWORD_DEFAULT);
            // construct query
            $reg_query = "INSERT INTO accounts 
            (email,username,password,created,updated,lastlogin,active)
            VALUES ('$cleanemail','$cleanuser','$hash',NOW(),NOW(),NOW(),1)";
            if(!$connection->query($reg_query)){
                $errors["reg_error"] = "error, cannot create account";
            } else {
                //---------log user in
                // get the user id from database
                $account_query = "SELECT account_id,email,username FROM accounts 
                WHERE email='$cleanemail'";
                $account_result = $connection->query($account_query);
                $userdata = $account_result->fetch_assoc();
                //create session variables from user data
                $_SESSION["user_id"] = $userdata["account_id"];
                $_SESSION["user_email"] = $userdata["email"];
                $_SESSION["user_name"] = $userdata["username"];
                // redirect to account page
                header("location:account.php");
            }   
        }
        
    }
}
?>
<!doctype html>
<html>
    <?php include("includes/head.php");?>
    <body>
        <div class="container">
            <div class="row">
                <h2 class="col-md-12 text-center">Register</h2>
                <div class="col-md-4 col-md-offset-4">
                    <form id="registration" method="post" action="">
                        <?php 
                            if($errors["email"]){ $emailerror = "has-error"; }
                        ?>
                        <div class="form-group <?php echo $emailerror; ?>">
                            <label for="email">Email</label>
                            <input type="email" 
                            class="form-control" 
                            name="email" 
                            id="email"
                            required
                            placeholder="you@domain.com"
                            <?php echo "value=$useremail";?>>
                            <span class="help-block">
                                <?php echo $errors["email"]; ?>
                            </span>
                        </div>
                        <?php 
                            if($errors["username"]){ $usernameerror = "has-error"; }
                        ?>
                        <div class="form-group <?php echo $usernameerror; ?>">
                            <label for="username">Username</label>
                            <input type="text" 
                            class="form-control" 
                            name="username" 
                            id="username" 
                            required
                            <?php echo "value=$username";?>>
                            <span class="help-block">
                                <?php echo $errors["username"]; ?>
                            </span>
                        </div>    
                        <?php 
                        if($errors["password"]){$pwerror = "has-error"; }
                        else{$pwerror = "has-success"; }
                        ?>
                        <div class="form-group <?php echo $pwerror; ?>">
                            <label for="password">Password</label>
                            <input 
                            type="password" 
                            class="form-control" 
                            name="password" 
                            id="password" 
                            required
                             <?php echo "value=$password";?>>
                            <span class="help-block">
                                 <?php echo $errors["password"];?>
                            </span>
                        </div>  
                        <div class="text-center">
                            <button type="submit" name="register" class="btn btn-info">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
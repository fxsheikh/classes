<?php
include("includes/functions.php");
// capture any get variables

if (count($_GET["categories"]) > 0){
    // get selected catgories
    $selected_cats = filter_var_array($_GET["categories"],FILTER_SANITIZE_NUMBER_INT);
    // print_r($selected_cats);
    
}

// capture selected maximum price
if($_GET["price"]){
    $selected_price = filter_var($_GET["price"],FILTER_SANITIZE_NUMBER_INT);
}

include("includes/database.php");

$cat_query = "SELECT category_id, category_name from categories WHERE active=1";
$cat_result = $connection->query($cat_query);

$categories = array();

if($cat_result->num_rows > 0){
  while($cat_row = $cat_result->fetch_assoc())
  {
    array_push($categories,$cat_row);
  }  
}

// get products from the database
$product_query = "SELECT products.id,products.name,products.price,images.image_file 
FROM products 
LEFT JOIN products_images 
ON products_images.product_id = products.id
LEFT JOIN images ON products_images.image_id = images.image_id";

// add category filters
$category_filter = " " . "INNER JOIN products_categories
ON products.id = products_categories.product_id";

// count selected categories
$cat_count = count($_GET["categories"]);

if($cat_count > 0){
    for($i=0;$i<$cat_count;$i++){
        
        $cat_id = $_GET["categories"][$i];
        
        if($i==0){
            $category_filter = $category_filter . " " . 
            "WHERE (products_categories.category_id = '$cat_id'";
        }
        else {
            $category_filter = $category_filter . " " . 
            "OR products_categories.category_id = '$cat_id'";
        }
        
        if($i == $cat_count-1){
            $category_filter = $category_filter . ") ";
        }
    }
    
    // echo $category_filter;
    $product_query = $product_query.$category_filter;
    
    
}

//filter by price
if($selected_price){
    if($cat_count > 0) {
        $price_filter = " "." AND products.price <= '$selected_price'";
    }
    else
    {
        $price_filter = " ". "WHERE products.price <= '$selected_price'";
    }
        $product_query = $product_query . $price_filter;
}

//set grouping of product results
$product_query = $product_query ." " . "GROUP BY products.id";

// PAGINATION -----------------------------------------------
// receive the page GET request or if none, set to 1
if($_GET["page"]){
    // we expect page number to be an integer, so we sanitise it as an integer
    $page = filter_var($_GET["page"],FILTER_SANITIZE_NUMBER_INT);
}
else {
    $page = 1;
}

//run the query to get total results
$products_total_result = $connection->query($product_query);

//store the total number of results
$totalrecords = $products_total_result->num_rows;

//set items per page
$itemsperpage = 12;

//get total number of pages using ceil, eg if last page
//has less than 12 items it still counts as a page
$totalpages = ceil($totalrecords/$itemsperpage);

$offset = ($page-1) * $itemsperpage;

//modify the query and add LIMIT and OFFSET for pagination of results
$product_query = $product_query . " " . "ORDER BY products.id ASC LIMIT $itemsperpage OFFSET $offset";

// echo $product_query;

$products_result = $connection->query($product_query);
$products = array();
if($products_result->num_rows > 0){
    while($products_row = $products_result->fetch_assoc()){
        array_push($products, $products_row);
    }
}

?>

<!doctype html>

<html>
    
    <?php include("includes/head.php"); ?>
    
    <body>
        

        
        
        <div class="container">
            
            <div class="row">
             
                 <aside class="col-md-2">
                     <!--left sidebar-->
                     
                     <?php include("includes/products_filters.php"); ?>
                     
                 </aside>
                
                <!--content area-->
                 <div class="col-md-10">
                     
                      <?php include("includes/pagination.php"); ?>
                      
                     <!--<div class="row">-->
                         <?php
                            
                            $productcount = count($products);
                            $counter = 0;
                            
                            for($i=0;$i<$productcount;$i++) {
                             
                             $counter++;
                             
                             if($counter==1){
                                echo "<div class=\"row\">";
                             }
                             
                             $id = $products[$i]["id"];
                             $name = $products[$i]["name"];
                             $price = $products[$i]["price"];
                             $image = $products[$i]["image_file"];
                             
                             echo "<div class = \"col-md-3\">
                             
                             <h4>$name</h4>
                             <img class=\"img-responsive\" src=\"images/$image\">
                             <p class=\"price\">$price</p>
                             <a class=\"btn btn-info \"  href=\"view.php?id=$id\">View Detail</a>
                             <hr>
                             </div>";

                            if($counter==4 || $i==$productcount-1) {
                                echo "</div>";
                                $counter = 0;
                            }
                            
                            }

                         
                         ?>
                     <!--</div>-->
                     
                      <?php include("includes/pagination.php"); ?>
                 </div>
                 
            </div>
            
        </div>
    </body>
    
    
</html>
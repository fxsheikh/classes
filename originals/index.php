<?php 

// get selected category
$selected = $_GET["category"];
// sanitize the variable
$selected = filter_var($selected,FILTER_SANITIZE_NUMBER_INT);


include("includes/database.php");
//$query = "SELECT products.id,products.name FROM products";
$query = "SELECT products.id,products.name, images.image_file
FROM products LEFT JOIN products_images ON products.id = products_images.product_id
LEFT JOIN images ON products_images.image_id = images.image_id";

// append the query to restrict category

if(isset($selected) && $selected != NULL) {
    
    $query = $query . " INNER JOIN products_categories
    ON products.id = products_categories . product_id
    WHERE products_categories.category_id = '$selected' ";
    
}

$query = $query . " GROUP BY products.id ORDER BY products.id ASC";

// echo $query;

$result = $connection->query($query);
$products = array();
if($result->num_rows > 0){
  while($row = $result->fetch_assoc())
  {
    array_push($products,$row);
  }  
}

// categories
$cat_query = "SELECT category_id,category_name FROM categories WHERE active=1";

$cat_result = $connection->query($cat_query);
$categories = array();

if($cat_result->num_rows > 0){
    while($cat_row = $cat_result->fetch_assoc())
    {
        array_push($categories,$cat_row);
    }
}

?>
<!doctype html>
<html>
    <?php include("includes/head.php"); ?>
    
    <body>
        
        <div class="container">
            
            <h1>Welcome to Home!</h1>
            
            <form id="search-form" method="get" action="search.php">
                <input type="text" placeholder="search words" name="query">
                <button type="submit" name="submit">Search</button>
            </form>
            
            <div class="row">
                <aside class="col-md-2">
                    
                    <h4>Categories</h4>
                    
                    <ul class="nav nav-stacked nav-pills">
                        
                        <?php 
                            //all categories
                            
                            if(!$selected) {
                                echo "<li class=\"active\">";
                            } else {
                                echo "<li>";
                            }
                            
                            echo "<a href=\"index.php\">All categories</a>";
                            echo "</li>";
                            
                            // end of all categories
                            
                            foreach ($categories as $cat_item) {
                               $cat_id = $cat_item["category_id"];
                               $cat_name = $cat_item["category_name"];
                               
                               // indicate the current category
                               if($selected==$cat_id){
                                   echo "<li class=\"active\">";
                               } else {
                                   echo "<li>";
                               }
                               
                               //end of indicator
                               echo "<a href=\"index.php?category=$cat_id\">$cat_name</a>";
                               echo "</li>";
                               
                            }
                        ?>
                        
                    </ul>
                    
                </aside>
                
                
                <main class="col-md-10">
                    
                   
                        <?php
                        $productnumbers = count($products);
                        
                        $counter = 0;
                        
                        for($i=0;$i<$productnumbers;$i++)
                        {
                            $productname = $products[$i]["name"];
                            $productid = $products[$i]["id"];
                            $productimage = $products[$i]["image_file"];
                            
                            $counter++;
                            
                            if($counter==1) {
                                echo "<div class=\"row front-product-row\">";
                            }
                            
                            echo "<div class=\"col-md-3 front-product\">";
                            echo "<h3 class=\"productname\">$productname</h3>";
                            
                            if($productimage) {
                                echo "<img class=\"img-responsive\" src=\"images/$productimage\">";
                            }
                            
                            echo "<a class=\"btn btn-info\" href=\"view.php?id=$productid\">View Detail</a>";
                            // echo "<hr>";
                            echo "</div>";
                            
                            if($counter==4 || $i==$productnumbers-1) {
                                echo "</div>";
                                $counter = 0;
                            }
                        }
                        ?>                       

                </main>
            </div>
            

        
        </div>

    </body>
</html>

<?php
//capture GET variable called token

print_r($_GET);

//if no token exit
if(!$_GET["token"]){
    // exit();
}
else {
    $token = $_GET["token"];
}
if(!ctype_alnum($token)){
    exit();
}

if($_SERVER["REQUEST_METHOD"]=="GET" || $_SERVER["REQUEST_METHOD"]=="POST"){
    //check token with database and see if token is in password_reset table and active
    $token_query = "SELECT account_id,token,created FROM password_reset WHERE token='$token' AND active=1";
    include("includes/database.php");
    $token_result = $connection->query($token_query);
    if($token_result->num_rows > 0){
        $row = $token_result->fetch_assoc();
        $stored_token = $row["token"];
        $created = $row["created"];
        $account_id = $row["account_id"];
        
        echo "<br>" . "date created is " . $created . "<br>";
        
        $start_date = new Datetime($created);
        // echo $start_date->format('m-d-Y h:i:s');
        
        $now_date = new Datetime();
        // echo $now_date->format('m-d-Y h:i:s');
        
        $interval = $start_date->diff($now_date);
        
        $minutes = $interval->days * 24 * 60;
        $minutes += $interval->h * 60;
        $minutes += $interval->i;

    echo "Interval time is " . $minutes . " minutes";

    }
}

//if token is more than two hours generate message
if ($minutes > 130){
    echo "The token is too old";
      //redirect to password reset page
    header("location:password-reset.php");
    echo "The token is too old";
}

// HOW TO SEND MESSAGE BEFORE REDIRECT (token has expired, and successful password change)
// PASSWORD BLANK logic error
// IS POST OR GET the right way to go about it

//perform password validation and checks 
if($_SERVER["REQUEST_METHOD"]=="POST"){
  //store validation errors in array
  $errors = array();
    //--check passwords
  //see if passwords have been submitted
  $password1 = $_POST["password1"];
  $password2 = $_POST["password2"];
  
    if(strlen($password1) == 0 || strlen($password2) == 8){
      $errors["password"] = "password needs to be at least 8 characters long";
    }
    
  //if passwords are not empty
  if($password1 || $password2){
    $password_errors = array();
    //if passwords are not equal log errors
    if($password1!==$password2){
      array_push($password_errors,"passwords are not the same");
    }
    //if passwords are less than 8 characters
    if(strlen($password1)<8 || strlen($password2)<8){
      array_push($password_errors,"password needs to be at least 8 characters long");
    }
    //if passwords do not contain numbers
    if($password1 == $password2 && !strpbrk($password1,"1234567890")){
      array_push($password_errors,"password needs to contain numbers");
    }
    //if there are errors combine errors messages
    if(count($password_errors) > 0){
      $errors["password"] = implode(" and ",$password_errors);
    }
  }
  
  //if password meets criteria then store and direct to login page
  
  if ($password1 == $password2 && count($errors) == 0){
    //redirect to password reset page
    header("location:login.php");
}
  
  
}

?>

<!--if it is and it is no more than 2 hours old then ask user for new password -->

<!doctype html>
<html>
    <?php include("includes/head.php"); ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form id="login-form" method="post" action="<?php echo "change-password.php?token=$token"?>">
                        <h3>Change Password</h3>

                            <!--password block-->
                            <?php
                            if($errors["password"]){
                              $password_class="has-error";
                              $password_message=$errors["password"];
                            }
                            ?>
            
                            <div id="password-group" class="form-group <?php echo $password_class; ?>">
                              <label for="password1">New Password</label>
                              <input type="password" name="password1" id="password1" class="form-control">
                              <label for="password2">Retype New Password</label>
                              <input type="password" name="password2" id="password2" class="form-control">
                              <span class="help-block"><?php echo $password_message; ?></span>
                            </div>

                        <div class="text-center">
                              <button type="submit" name="submit" value="submit" class="btn btn-info btn-block">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
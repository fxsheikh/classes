<?php 
include("includes/database.php");
//receive post data
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $reset_email = filter_var($_POST["reset-email"],FILTER_SANITIZE_EMAIL);
    //create errors array 
    $errors= array();
    //validate the email
    if(!filter_var($reset_email,FILTER_VALIDATE_EMAIL)){
        $errors["validity"] = "not a valid email address";
    }
    //check if user exists in database
    if(count($errors)==0){
        // create query
        $reset_query = "SELECT account_id,email FROM accounts WHERE email='$reset_email'";
        $result = $connection->query($reset_query);
        if($result->num_rows == 0){
            $errors["exists"] = "account does not exist";
        }
        else {
            //account id and email
            $userdata = $result->fetch_assoc();
            $account_id = $userdata["account_id"];
            $account_email = $userdata["email"];
            //proceed to reset password
            //generate a random token
            $token = bin2hex(openssl_random_pseudo_bytes(16));
            //insert into password_reset table
            $pw_reset_query = "INSERT INTO password_reset 
            (account_id,token,created,active) VALUES ('$account_id','$token',NOW(),1)";
            if(!$connection->query($pw_reset_query)){
                $errors["database"] = "database error";
            }
            else{
                // generate email
                $reset_link = "http://".$_SERVER["SERVER_NAME"]."/change-password.php"."?token=$token";
                $reset_message = "Someone has requested a password reset. Click on this link
                to reset your password <a href=\"$reset_link\">$reset_link</a>";
                $headers = "MIME-Version: 1.0". "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "fxsheikh87@gmail.com" . "\r\n";
                
                $subject = "Password Reset";
                if(!mail($account_email,$subject,$reset_message,$headers)){
                    $errors["send"] = "error sending email";
                }
                else {
                    $success = "an email has been sent to your registered address";
                }
                // generate success message
                echo $success;
            }
        }
    }
}
$page_title = "Reset your password";


?>
<!doctype html>
<html>
<?php include("includes/head.php");?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h3>Reset Password</h3>
                <form id="reset-form" action="password-reset.php" method="post">
                    <?php 
                    if($errors["validity"] || $errors["exists"]){
                        $error_class = "has-error";
                        $combined_error = implode(" ",$errors);
                    }
                    ?>
                    <div class="form-group <?php echo $error_class; ?>">
                        <label for="reset-email">Your Email Address</label>
                        <input type="email" 
                        name="reset-email" 
                        id="reset-email"
                        class="form-control"
                        placeholder="you@domain.com">
                        <span class="help-block"><?php echo $combined_error; ?></span>
                    </div>

                    <div class="text-center">
                        <button class="btn btn-warning" type="submit" name="submit">Reset Your Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
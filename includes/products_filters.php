 <form id ="product-filter" method="get">
                         <h4>Filter by Category</h4>
                        <?php 
                        
                            foreach($categories as $cat_item)
                            { 
                                $cat_id = $cat_item["category_id"];
                                $cat_name = $cat_item["category_name"];
                                
                                // check if a category has been selected and set status to "checked" if it has
                                
                                $status="";
                                
                                if(count($selected_cats) > 0){
                                    
                                    foreach($selected_cats as $cat){
                                        if($cat == $cat_id){
                                            $status = "checked";
                                        }
                                    } 
                                }
                                

                                
                                
                                echo 
                                    "<div class=\"checkbox\">
                                    
                                    <label>
                        
                                        <input  type=\"checkbox\" 
                                                name=\"categories[]\" 
                                                value=\"$cat_id\" $status>
                                        $cat_name
                        
                                    </label>
                                    
                                    </div>";
                            }
                            
                        ?>    
                         
                         <h4>Filter by price</h4>
                         <label>Maximum Price</label>
                         
                         <?php 
                            if($selected_price){
                                $initprice = $selected_price;
                            }
                            else {
                                $initprice = 250;
                            }
                            
                            // get minimum value from the products table
                            $minquery = "SELECT MIN(price) FROM products";
                            $minresult = $connection->query($minquery);
                            $min = $minresult->fetch_assoc();
                            $minprice = floor($min["MIN(price)"]);
                            
                            // get maximum value from products table
                            $maxquery = "SELECT MAX(price) FROM products";
                            $maxresult = $connection->query($maxquery);
                            $max = $maxresult->fetch_assoc();
                            $maxprice = ceil($max["MAX(price)"]);                            
                            
                         ?>
                         <input 
                            type="range" 
                            id="price-range" 
                            name="price" 
                            min="<?php echo $minprice; ?>" 
                            max="<?php echo $maxprice; ?>" 
                            step="1" 
                            name="price" 
                            value="<?php echo $initprice; ?>"
                         >

                         <div class="range-label">
                             <label><?php echo $minprice; ?></label>
                             <label><?php echo $maxprice; ?></label>
                         </div>             
                         
                         <input type="text" id="price-display" name="price-display" readonly class="form-control">
                         
                         <button class="btn btn-info" type="submit" name="submit">
                             Submit
                         </button>
                         
                         <button class="btn btn-info" type="reset" name="reset">
                             Clear
                         </button>   
                         
</form>
<head>
        <title><?php echo $page_title; ?></title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="style.css">
        <script src="components/jquery/dist/jquery.min.js"></script>
        <script src="components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="js/main.js"></script>
</head>
<div class="row">
    <div class="col-md-8">
        <nav>
            <ul class="pagination">
                <?php
                    //---- Previous page
                    if($page>1){
                        $previous_page = $page-1;
                        $previous_url = generateURL($previous_page,$selected_cats,$selected_price);
                    }
                
                    $disabled_button_class = "disabled";
                    //if at first page disable previous button
                    if($page==1){
                        $disabler = $disabled_button_class;
                    }
                    else{
                        $disabler = "";
                    }
                    echo "<li class=\"$disabler\">
                        <a href=\"$previous_url\" aria-label=\"Previous\">
                            <span aria-hidden=\"true\">&laquo;</span>
                        </a>
                        </li>";
                        
                    for($h=0;$h<$totalpages;$h++){
                        //generate page numbers starting from 1
                        $pagenumber = $h+1;
                        if($pagenumber==$page){
                            //if a page number matches current page, highlight with active
                            $active = "active";
                        }
                        else{
                            $active = "";
                        }
                        // generate url for each page number
                        $pageurlnumber = $h+1;
                        $url = generateURL($pageurlnumber,$selected_cats,$selected_price);
                        echo "<li class=\"$active\"><a href=\"$url\">$pagenumber</a></li>";
                    }    
                    
                    //next pointer
                    if ($page==$totalpages){
                        $disabler = $disabled_button_class;
                    }
                    else {
                        $disabler = "";
                    }
                    
                    if($page<$totalpages){
                        $next_page = $page + 1;
                        $next_page_url = generateURL($next_page,$selected_cats,$selected_price);
                    }
                    
                    
                    echo "<li class=\"$disabler\">
                        <a href=\"$next_page_url\" aria-label=\"Next\">
                            <span aria-hidden=\"true\">&raquo;</span>
                        </a>
                        </li>";                    
                    
                ?>
            </ul>
        </nav>
    </div>
    <div class="col-md-4 text-right">
        <span class="pagination pagination-text">
            <?php echo "Page $page of $totalpages"; ?>
        </span>
        <span class="pagination pagination-text">
             <?php echo "$totalrecords total products"; ?>
        </span>
    </div>
</div>
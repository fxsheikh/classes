<?php
function generateURL($pagenumber,$selected_cats,$selected_price){
    $baseurl = $_SERVER["PHP_SELF"];
    $baseurl = $baseurl."?"."page=".$pagenumber;
    //categories selected
    $cat_count = count($selected_cats);
    if($cat_count>0){
        foreach($selected_cats as $category){
             $cat_url = "&".urlencode("categories[]")."=".$category;
             $baseurl = $baseurl.$cat_url;
        }
    }
    if($selected_price){
        $baseurl = $baseurl."&"."price=".$selected_price;
    }
    return $baseurl;
}
?>